import React from 'react';
import ReactDOM from 'react-dom';
import { HeroesApp } from './HeroesApp';
import  'animate.css'
import './components/styles/style.scss';




ReactDOM.render(
    <HeroesApp />
    ,
  document.getElementById('root')
);


