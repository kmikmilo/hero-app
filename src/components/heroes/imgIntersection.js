import React, { useEffect, useRef, useState } from 'react'

const Img = ({ id }) => {
    const registerObserver = (ref, setShowImage) => {
        const observer = new IntersectionObserver((entries, observer) => {
            entries.forEach(entry => {
                if (!entry.isIntersecting) {
                    return;
                }
                setShowImage(true);
                observer.disconnect();
            });
        });
        observer.observe(ref);
    }

    const [showImage, setShowImage] = useState(false);
    const imageRef = useRef(null);

    useEffect(() => {
        registerObserver(imageRef.current, setShowImage);
    }, [])
    return (
        
        <>
            {showImage ? <img src={`./assets/heroes/${id}.jpg`} className="col-6 animate__animated animate__fadeIn" alt={id} /> : <span className={'image-style'}  ref={imageRef}></span>}
        </>
    )
}
export default Img;