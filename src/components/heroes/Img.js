import useOnScreen from '../../hooks/useOnScreen';

const Img = ({ id }) => {
    const [setRef,visible] =  useOnScreen({threshold: 1});
   
    return (
        
        <>{visible?
          <img src={`./assets/heroes/${id}.jpg`} className="col-6 animate__animated animate__fadeIn" alt={id} />
          :<span ref={setRef}></span> } 
        </>
    )
}
export default Img;