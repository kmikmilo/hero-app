import { Link } from 'react-router-dom'
 import  Img  from './Img';
 import useOnScreen from '../../hooks/useOnScreen'
// const Img = lazy(()=>import('./Img'));
 const HeroCard = ({
    id,
    superhero,
    publisher,
    alter_ego,
    first_appearance,
    characters }) => {


       const [setRef,visible] =  useOnScreen({threshold: 0.99});
    return (
        <>
       
        <div ref={setRef} className="col-sm-4 responsive">
             { visible?(
            <div className="card" style={{ marginBottom: 30 }}>
                <div className="row">
                    {/* <img src={`./assets/heroes/${id}.jpg`} className=" col-6" alt={id} /> */}

                    <Img id={id}/>
                    <div className="col-6 text-center">
                        <div className="card-body col-15" style={{ minHeight: 150 }}>
                            <h5 className="card-title">{superhero}</h5>
                            <p className="card-text">
                                {characters.substr(0, 10) + '...'}
                            </p>
                            {/* {
                            (alter_ego!== characters)
                            &&
                            <p className="card-text ">
                                {characters}
                            </p>
                        } */}
                            <Link to={`./hero/${id}`}>mas...</Link>
                        </div>
                    </div>
                </div>
            </div>)
            : <span ref={setRef}></span>}
        </div>
    
</>
    )
}

 export default HeroCard;
