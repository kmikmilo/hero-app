import React, { Suspense, useMemo } from 'react'
import { getHerosByPublisher } from '../../selectors/getHerosByPublisher.js'
// import  HeroCard  from './HeroCard'
const HeroCard = React.lazy(()=>import ('./HeroCard'))

export const HeroList = ({ publisher }) => {
    // const heros = getHerosByPublisher(publisher)

    //si cambi l publisher entones se renderiza de nuevo, de lo contrario se carga la info guardada
    const heros =useMemo(() => getHerosByPublisher(publisher), [publisher])

    return (
        <div className="row animate__animated animate__fadeIn" style={{justifyContent:'center'}}>
                {

                        heros.map(hero => (
                            <Suspense fallback={<h1>loading...</h1>}
                                key={hero.id}
                            >
    
                            <HeroCard key={hero.id}
                                {...hero}
                            />
                    </Suspense>

                        ))
                    }
            </div>
    )
}
