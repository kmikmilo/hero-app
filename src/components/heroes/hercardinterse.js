import React, { useEffect, useState } from 'react'
import { Link } from 'react-router-dom'
 import  Img  from './Img';
// const Img = lazy(()=>import('./Img'));
function useOnScreen(options) {
    const [ref,setRef] = useState(null);
    const [visible, setVisible] = useState(false);

    useEffect(() => {
        const observer = new IntersectionObserver(([entry])=> {
            setVisible(entry.isIntersecting);
        },options);

        if(ref)
        observer.observe(ref)
        return () => {
            if(ref){
                observer.unobserve(ref);
            }
        }
    }, [ref,options])
    return [setRef,visible]
}


 const HeroCard = ({
    id,
    superhero,
    publisher,
    alter_ego,
    first_appearance,
    characters }) => {


       const [setRef,visible] =  useOnScreen({threshold: 0.2});
    return (
        <>
       
        <div ref={setRef} className="col-sm-4 responsive">
             { visible?(
            <div className="card" style={{ marginBottom: 30 }}>
                <div className="row">
                    {/* <img src={`./assets/heroes/${id}.jpg`} className=" col-6" alt={id} /> */}

                    <Img id={id}/>
                    <div className="col-6 text-center">
                        <div className="card-body col-15" style={{ minHeight: 150 }}>
                            <h5 className="card-title">{superhero}</h5>
                            <p className="card-text">
                                {characters.substr(0, 10) + '...'}
                            </p>
                            {/* {
                            (alter_ego!== characters)
                            &&
                            <p className="card-text ">
                                {characters}
                            </p>
                        } */}
                            <Link to={`./hero/${id}`}>mas...</Link>
                        </div>
                    </div>
                </div>
            </div>)
            : <p ref={setRef}></p>}
        </div>
    
</>
    )
}

 export default HeroCard;
