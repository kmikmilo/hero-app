import { useEffect, useReducer } from 'react'
import { AuthContext } from './auth/AuthContext'
import { AppRouter } from './routers/AppRouter'
import { authReducer } from './auth/authReducer'


//funcion para evalaur el local storege
const init = () => {
    //si existe retorn el storage(el user y password) y si no retorna logged false
    console.log(JSON.parse(localStorage.getItem('user')))
    return JSON.parse(localStorage.getItem('user')) || {
        logged: false
    };
}

export const HeroesApp = () => {
    //                                   reducer,initialState,logged:false
    const [user, dispatch] = useReducer(authReducer, {}, init);
    useEffect(() => {
        localStorage.setItem('user', JSON.stringify(user))
    
    }, [user]);
    return (
        //le estoy pasando el reducer a traves de un context a toda la aplicacion debido a que es un high order component   
        <AuthContext.Provider value={{user,dispatch}}>
            <AppRouter />
        </AuthContext.Provider>

    )
}
